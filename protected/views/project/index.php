<?php
$this->pageTitle=Yii::app()->name . ' - 项目列表';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>项目列表</h1>
			</div>
		</div>
		<div class="MetadataView">
		
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'project-grid',
			'htmlOptions'=>array(
					'class'=>'cgrid-view'
			),
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array('header'=>'项目名称', 'name'=>'projectName'),
				array('header'=>'SVN项目名', 'name'=>'svnProjectName'),
				array('header'=>'SVN路径', 'name'=>'svnProjectPath'),
				array(
					'header'=>'操作',
					'class'=>'CButtonColumn',
				),
			),
		)); ?>
		
		</div>
	</div>
</div>