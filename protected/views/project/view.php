<?php
$this->pageTitle=Yii::app()->name . ' - 查看项目';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>查看项目 <?php echo $model->projectName; ?></h1>
			</div>
		</div>
		<div class="MetadataView">
      <div class="view-toolbar">
          <?php echo CHtml::link('返回', array('/project/index'));?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'projectName'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'projectName'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'svnProjectPath'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'svnProjectPath'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'svnProjectName'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'svnProjectName'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'isRemove'); ?></th>
              <td colspan="1">
              <?php echo $model->isRemove==1?'是':'否'; ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'svnUser'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'svnUser'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'svnPass'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'svnPass'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'noPubList'); ?></th>
              <td colspan="1">
              <?php
              	echo nl2br(CHTML::value($model, 'noPubList', '&nbsp;'));
              ?>
              </td>
              <th>-</th>
               <td colspan="1">-</td>
            </tr>
            <?php 
            //可用测试服务器
            $testHosts = array();
            //可用生成服务器
            $prodHosts = array();
            if (isset($model->hosts) && is_array($model->hosts)) {
            	foreach($model->hosts as $host) {
            		if ($host->hostType==1) {
            			$prodHosts[$host->hostId] = $host->hostText;
            		}
            		else {
            			$testHosts[$host->hostId] = $host->hostText;
            		}
            	}
            
            }
            ?>
            <tr>
              <th><?php echo CHtml::label('测试服务器','testHostIds');?></th>
              <td colspan="3">
              <?php 
              	if (is_array($testHosts)) :
              		$no = 1;
              		foreach ($testHosts as $key => $value) :
              			echo CHtml::link($no.':'.$value, array('host/view', 'id'=>$key));
              			echo '&nbsp;';
              			$no++;
              		endforeach;
              	endif;
              ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::label('生成服务器','prodHostIds');?></th>
              <td colspan="3">
              <?php 
              	if (is_array($prodHosts)) :
              		$no = 1;
              		foreach ($prodHosts as $key => $value) :
              			echo CHtml::link($no.':'.$value, array('host/view', 'id'=>$key));
              			echo '&nbsp;';
              			$no++;
              		endforeach;
              	endif;
              ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::label('svn提交钩子','svnhook');?></th>
              <td colspan="3">
              <?php 
              $post_commit_path = Yii::getPathOfAlias('application.data').'/post-commit';
              $template = file_get_contents($post_commit_path);
              $template = strtr($template, array('{{projectId}}'=>$model->projectId, '{{svnProjectName}}'=> $model->svnProjectName));
              
              echo CHtml::textArea('svnhook', $template, array('rows'=>50, 'cols'=>100));
              ?>
              </td>
            </tr>
          </tbody>
        </table>
		
		</div>
	</div>

</div>
