<?php
$this->pageTitle=Yii::app()->name . ' - 添加项目';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>添加项目</h1>
			</div>
		</div>
		<div class="MetadataView">

		<?php echo $this->renderPartial('_form', 
			array(
				'model'=>$model,
				'testHostIds'=>$testHostIds,
				'prodHostIds'=>$prodHostIds,
			    'prodHostListData'=>$prodHostListData,
				'testHostListData'=>$testHostListData
			)); ?>
		
		</div>
	</div>

</div>