    <div class="wide form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'user-form',
                'enableAjaxValidation'=>false,
        )); ?>
      <div class="view-toolbar">
          <?php echo CHtml::link('取消', array('/project/index'));?>&nbsp;|&nbsp;
          <?php echo CHtml::submitButton($model->isNewRecord ? '添加' : '保存'); ?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo $form->labelEx($model,'projectName'); ?></th>
              <td colspan="1">
              <?php echo $form->textField($model,'projectName',array('size'=>60,'maxlength'=>50)); ?>
              <br />项目名称，用于区分不同的项目
              <?php echo $form->error($model,'projectName'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'svnProjectPath'); ?></th>
              <td colspan="1">
              <?php echo $form->textField($model,'svnProjectPath',array('size'=>60,'maxlength'=>50)); ?>
              <br />SVN项目路径，最终checkout的路径，如svn://localhost/help或svn://localhost/help/trunk等
              <?php echo $form->error($model,'svnProjectPath'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'svnProjectName'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'svnProjectName',array('size'=>60,'maxlength'=>100)); ?>
				<br />SVN项目名称，用于区分同仓库下的不同路径，如果svn://localhost/help/trunk
				<br />help为项目名，trunk为目录名,则此处填写trunk,如果svn://localhost/help/branches/br-help
				<br />则此处填写branches/br-help
				<?php echo $form->error($model,'svnProjectName'); ?>
              </td>
              <th><?php echo $form->label($model,'isRemove'); ?></th>
              <td colspan="1">
              	<?php echo $form->checkBox($model,'isRemove', array('value'=>1, 'checked'=>$model->isRemove==1?true:false)); ?>
				<br />是否从发布服务器中，删除svn中已删除的文件
			</td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'svnUser'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'svnUser',array('size'=>60,'maxlength'=>100)); ?>
				<br />项目的svn帐号
				<?php echo $form->error($model,'svnUser'); ?>
              </td>
              <th><?php echo $form->label($model,'svnPass'); ?></th>
              <td colspan="1">
              	<?php echo $form->textField($model,'svnPass',array('size'=>60,'maxlength'=>100)); ?>
				<br />项目的svn密码
				<?php echo $form->error($model,'svnPass'); ?>
			</td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'noPubList'); ?></th>
              <td colspan="1">
				<?php echo $form->textArea($model,'noPubList',array('rows'=>6, 'cols'=>50, 'style'=>'width:515px;height:150px;')); ?>
				<br />不需要发布到发布服务器的文件/目录列表，回车分开
              </td>
              <th>-</th>
              <td colspan="1">
              	-
			</td>
            </tr>
            
            <tr>
              <th><?php echo CHtml::label('测试服务器','testHostIds[]');?></th>
              <td colspan="1">
				<?php echo CHtml::dropDownList('testHostIds[]',
						$testHostIds,
						$testHostListData,
						array(
							'multiple'=>true,
						        'style'=>'width:520px;height:100px;'	
						)
					);?>
              </td>
              <th><?php echo CHtml::label('生成服务器','prodHostIds[]');?></th>
              <td colspan="1">
              		<?php echo CHtml::dropDownList('prodHostIds[]',
						$prodHostIds,
						$prodHostListData,
						array(
							'multiple'=>true,
						        'style'=>'width:520px;height:100px;'	
						)
					);?>
			 </td>
            </tr>
          </tbody>
        </table>
    </div>
    <?php $this->endWidget(); ?>
  </div>