<?php
$this->pageTitle=Yii::app()->name . ' - 添加用户';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>添加用户</h1>
			</div>
		</div>
		<div class="MetadataView">

		<?php echo $this->renderPartial('_form', 
			array(
				'model'=>$model,
				'projectListData'=>$projectListData,
				'prodHostListData'=>$prodHostListData,
			    'testHostListData'=>$testHostListData,
				'userProjectIds'=>$userProjectIds,
			    'userTestHostIds'=>$userTestHostIds,
			    'userProdHostIds'=>$userProdHostIds
			)); ?>
		
		</div>
	</div>

</div>