<?php
$this->pageTitle=Yii::app()->name . ' - 用户列表';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>用户列表</h1>
			</div>
		</div>
		<div class="MetadataView">
		
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'user-grid',
			'htmlOptions'=>array(
					'class'=>'cgrid-view'
			),
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array('header'=>'用户名', 'name'=>'username'),
				array('header'=>'邮箱', 'name'=>'email'),
				array(
					'header'=>'操作',
					'class'=>'CButtonColumn',
				),
			),
		)); ?>
		
		</div>
	</div>

</div>