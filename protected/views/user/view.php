<?php
$this->pageTitle=Yii::app()->name . ' - 查看用户';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>查看用户 <?php echo $model->username; ?></h1>
			</div>
		</div>
		<div class="MetadataView">
      <div class="view-toolbar">
          <?php echo CHtml::link('返回', array('/user/index'));?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'username'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'username'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'email'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'email'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::label('可用项目','userProjectIds');?></th>
              <td colspan="3">
              <?php 
              	if (isset($model->projects) && is_array($model->projects)) :
              		$no = 1;
              		foreach ($model->projects as $project) :
              			echo CHtml::link($no.':'.$project->projectName, array('project/view', 'id'=>$project->projectId));
              			echo '&nbsp;';
              			$no++;
              		endforeach;
              	endif;
              ?>
              </td>
            </tr>
            <?php 
            //可用测试服务器
            $userTestHosts = array();
            //可用生成服务器
            $userProdHosts = array();
            if (isset($model->hosts) && is_array($model->hosts)) {
            	foreach($model->hosts as $host) {
            		if ($host->hostType==1) {
            			$userProdHosts[$host->hostId] = $host->hostText;
            		}
            		else {
            			$userTestHosts[$host->hostId] = $host->hostText;
            		}
            	}
            
            }
            ?>
            <tr>
              <th><?php echo CHtml::label('可用测试服务器','userTestHostIds');?></th>
              <td colspan="3">
              <?php 
              	if (is_array($userTestHosts)) :
              		$no = 1;
              		foreach ($userTestHosts as $key => $value) :
              			echo CHtml::link($no.':'.$value, array('host/view', 'id'=>$key));
              			echo '&nbsp;';
              			$no++;
              		endforeach;
              	endif;
              ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::label('可用生成服务器','userProdHostIds');?></th>
              <td colspan="3">
              <?php 
              	if (is_array($userProdHosts)) :
              		$no = 1;
              		foreach ($userProdHosts as $key => $value) :
              			echo CHtml::link($no.':'.$value, array('host/view', 'id'=>$key));
              			echo '&nbsp;';
              			$no++;
              		endforeach;
              	endif;
              ?>
              </td>
            </tr>
          </tbody>
        </table>
		
		</div>
	</div>

</div>