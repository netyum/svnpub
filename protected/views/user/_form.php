    <div class="wide form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'user-form',
                'enableAjaxValidation'=>false,
        )); ?>
      <div class="view-toolbar">
          <?php echo CHtml::link('取消', array('/user/index'));?>&nbsp;|&nbsp;
          <?php echo CHtml::submitButton($model->isNewRecord ? '添加' : '保存'); ?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo $form->labelEx($model,'username'); ?></th>
              <td colspan="1">
              <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>50)); ?>
              <?php echo $form->error($model,'username'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'password'); ?></th>
              <td colspan="1">
              <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>50)); ?>
              <?php echo $model->isNewRecord ? '' : '<br />留空将不修改密码';?>
              <?php echo $form->error($model,'$password'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'email'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
				<?php echo $form->error($model,'email'); ?>
              </td>
              <th><?php echo CHtml::label('可用项目','userProjectIds[]'); ?></th>
              <td colspan="1">
              		<?php echo CHtml::dropDownList('userProjectIds[]',
						$userProjectIds,
						$projectListData,
						array(
							'multiple'=>true,
						        'style'=>'width:520px;height:100px;'	
						)
					);?>
			</td>
            </tr>
            <?php if (Yii::app()->user->username=='admin') :?>
            <tr>
              <th><?php echo $form->labelEx($model,'role'); ?></th>
              <td colspan="1">
				<?php echo $form->dropDownList($model,'role',array('admin'=>'管理员', 'user'=>'用户')); ?>
				<?php echo $form->error($model,'role'); ?>
              </td>
              <th>--</th>
              <td colspan="1">
              	--
			</td>
            </tr>
           <?php endif; ?>
            <tr>
              <th><?php echo CHtml::label('可用测试服务器','userTestHostIds[]');?></th>
              <td colspan="1">
				<?php echo CHtml::dropDownList('userTestHostIds[]',
						$userTestHostIds,
						$testHostListData,
						array(
							'multiple'=>true,
						        'style'=>'width:520px;height:100px;'	
						)
					);?>
              </td>
              <th><?php echo CHtml::label('可用生成服务器','userProdHostIds[]');?></th>
              <td colspan="1">
              		<?php echo CHtml::dropDownList('userProdHostIds[]',
						$userProdHostIds,
						$prodHostListData,
						array(
							'multiple'=>true,
						        'style'=>'width:520px;height:100px;'	
						)
					);?>
			 </td>
            </tr>
          </tbody>
        </table>
    </div>
    <?php $this->endWidget(); ?>
  </div>