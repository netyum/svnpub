<?php
$this->pageTitle=Yii::app()->name . ' - 查看发布日志';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>查看发布日志 <?php echo $model->project->projectName?>:<?php echo CHtml::encode($model->reason); ?></h1>
			</div>
		</div>
		<div class="MetadataView">
      <div class="view-toolbar">
          <?php echo CHtml::link('返回', array('/publog/index'));?>
          <?php echo CHtml::link('回滚', array('/publog/rollback'));?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'pubUser'); ?></th>
              <td colspan="1">
              <?php echo CHtml::value($model, 'pubUser'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'reason'); ?></th>
              <td colspan="1">
              <?php echo CHtml::value($model, 'reason'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'host'); ?></th>
              <td colspan="1">
              <?php echo CHtml::value($model, 'host'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'createTime'); ?></th>
              <td colspan="1">
              <?php echo CHtml::value($model, 'createTime'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'fileList'); ?></th>
              <td colspan="3">
              <?php
              echo nl2br($model->fileList); 
              ?>
              </td>
            </tr>
          </tbody>
        </table>
		
		</div>
	</div>

</div>