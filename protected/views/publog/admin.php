<?php
$this->breadcrumbs=array(
	'日志'=>array('index'),
	'管理',
);

$this->menu=array(
	array('label'=>'管理日志', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('log-grid', {
		data: $(this).serialize()
	});
	return false;
});

");

Yii::app()->clientScript->registerScript('rollback', "
function rollback(t) {
	var url = $(t).attr('href');
	var log_id =  url.substring( url.lastIndexOf('=')+1 );
	
	jQuery('#logid').html(log_id);
	jQuery('#mydialog').dialog('open');
	return false;
}

");
?>
<script>
function pub() {

	var reason = jQuery.trim(jQuery('#reason').val());
	if (reason=='') {
		alert('请填写发布原因'); jQuery('#reason')[0].focus(); return false;
	}


	if (!confirm('请确认，要发布此日志id对应的档案')) return false;
	
	jQuery('#pub_button').attr('disabled', true);
	
	jQuery('.ui-dialog-titlebar').last().remove();
	jQuery('#publoading').dialog("open");
	
	jQuery.ajax({
		'type':'post',
		'url':'<?php echo $this->createUrl('pub/rollback');?>',
		'data':{
			'log_id':jQuery('#logid').html(),
			'reason':reason,
			'host_id':jQuery('#host').val(),
			'project_id':<?php echo $defaultProjectId;?>
		},
		'dataType':'json',
		'success':function(data, status) {
			if (data.error) {
				alert(data.error);
			}
			else {
				alert('发布成功');
				location.href="<?php echo $this->createUrl('log/admin');?>";
			}
			jQuery('#publoading').dialog("close");
			jQuery('#pub_button').attr('disabled', false);
		}
	});
	
	/*
	jQuery('.select-on-check').each(function(){
		
	});*/
	//alert(1);
}
</script>


<h1>管理日志</h1>
<p>选择项目:
	<?php

	echo CHtml::dropDownList('选择项目', $defaultProjectId, $projectListData, 
		array('onchange'=>'location.href="'. $this->createUrl('admin').'&project_id="+this.value;'));
	?>
</p>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'log-grid',
	'dataProvider'=>$model->search($defaultProjectId),
	//'filter'=>$model,
	'columns'=>array(
		'log_id',
	    array('header'=>'发布原因','name'=>'reason'),
		array('header'=>'发布者','name'=>'author'),
		array('header'=>'档案名','name'=>'archive'),
	        array('header'=>'已发布主机', 'type'=>'raw', 'name'=>'host'),
		array('header'=>'发布时间', 'value'=>'date("Y-m-d H:i:s", $data->addtime)'),
		array(
		    'header'=>'操作',
			'class'=>'CButtonColumn',
		     //   'template'=>'{view} {delete} {pub}',
				'template'=>'{view} {delete}',
		        'htmlOptions'=>array('style'=>'width:150px'),
		        'buttons'=>array(
			   /* 
			   	edit by yanxiaowei@20110624: cancel the function :rollback
			    'pub'=>array(
				'label'=>'回滚',
				'click'=>'js:function(){ return rollback(this);}',
				'url'=>'Yii::app()->createUrl("pub/rollback", array("log_id"=>$data->log_id) )',
			    )
				*/
			)
		),
	),
)); ?>


<?php
 $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
      'id'=>'mydialog',
      // additional javascript options for the dialog plugin
      'options'=>array(
          'title'=>"回滚",
	  'resizable'=>false,
          'autoOpen'=>false,
	  'disabled'=>true,
	  'modal'=> true,
	  'closeOnEscape'=>false,
	  'draggable'=>false,
	  'width'=>500,
	  'height'=>300,
      ),
  ));?>
	<div class="row">
		<?php echo CHtml::label('日志id','log_id');?>
		<?php echo CHtml::tag("span", array('id'=>'logid'));?>
	</div>

	<div class="row">
		<?php echo CHtml::label('回滚原因','reason'); ?>
		<?php echo CHtml::textField('reason', '', array('id'=>'reason', 'size'=>30,'maxlength'=>100)); ?>
	</div>
	
	<div class="row buttons">
		<?php 
		echo CHtml::button('回滚到此版本', array('id'=>'pub_button', 'onclick'=>'pub()'));
		?>
	</div>

<?php

 $this->endWidget('zii.widgets.jui.CJuiDialog');
 ?>


<?php
 $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
      'id'=>'publoading',
      // additional javascript options for the dialog plugin
      'options'=>array(
          'title'=>false,
	  'resizable'=>false,
          'autoOpen'=>false,
	  'disabled'=>true,
	  'modal'=> true,
	  'closeOnEscape'=>false,
	  'draggable'=>false,
	  'height'=>60,
      ),
  ));

      echo '发布中...请稍候...';

 $this->endWidget('zii.widgets.jui.CJuiDialog');
 ?>
