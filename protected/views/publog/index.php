<?php
$this->pageTitle=Yii::app()->name . ' - '. $currentProject->projectName .'- 发布日志列表';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1><?php echo $currentProject->projectName;?> 发布日志列表</h1>
			</div>
		</div>
		<div class="MetadataView">
      	<div class="view-toolbar">
          <?php echo CHtml::button('选择其它项目', array('class'=>'select-project'));?>&nbsp;
          <?php echo CHtml::button('删除选中日志', array('class'=>'remove')); ?>&nbsp;
        </div>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'log-grid',
			'selectableRows'=>2,
			'htmlOptions'=>array(
					'class'=>'cgrid-view'
			),
			'dataProvider'=>$model->search($currentProject->projectId),
			'filter'=>$model,
			'columns'=>array(
					array(
							'class'=>'CCheckBoxColumn'
					),
					//array('header'=>'发', 'name'=>'filename', 'value'=>'Tools::checkPath($data->filename,"'. $currentProject->svnProjectName .'")'),
					array('header'=>'发布人','name'=>'pubUser'),
					array('header'=>'发布原因','name'=>'reason'),
					array('header'=>'发布主机', 'name'=>'host'),
					array(
							'header'=>'发布时间',
							'name'=>'createTime',
							'language'=>'zh_cn',
							'filter'=>'single',
							'class'=>'SYDateColumn'
					),
					array(
							'header'=>'操作',
							'class'=>'CButtonColumn',
							'template'=>'{view} {delete}',
							/*
							'htmlOptions'=>array(
									'style'=>'width:10%;'
							),
							'deleteButtonImageUrl'=>false,
							'viewButtonImageUrl'=>false,
							'buttons'=>array(
									'rollback'=>array(
											'label'=>'回滚',
											'url'=>'Yii::app()->controller->createUrl("rollback",array("id"=>$data->primaryKey))'
									)
							)*/
					),
			),
		)); ?>
		
		</div>
	</div>

</div>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'=>'select_project',
        // additional javascript options for the dialog plugin
        'options'=>array(
                'title'=>'选择项目',
                'resizable'=>false,
                'autoOpen'=>false,
                'disabled'=>true,
                'modal'=> true,
                'closeOnEscape'=>false,
                'draggable'=>false,
                'height'=>100,
        ),
)); 

foreach($projects as $p) :
	$url = $this->createUrl('index', array('id'=>$p->projectId));
	$link = CHtml::link($p->projectName, $url);
	echo CHtml::tag('span', array(), $link);
	echo '&nbsp;';
	echo CHtml::closeTag('span');
endforeach;

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<?php 
Yii::app()->clientScript->registerScript('upload', "
		var upload_ids = new Array();
		$('.select-project').live('click',function(){
			$('#select_project').dialog('open');
		});
		
		$('.remove').live('click',function(){
			var select_checked_num = jQuery('input.select-on-check:checked').length;
	
			if (select_checked_num==0) {
				alert('请选择要删除的发布日志');return false;
			}

            jQuery('input.select-on-check:checked').each(function() {
                upload_ids.push(jQuery(this).val());
            });
			if (!confirm('请确认，要删除选中的发布日志，删除发布日志也会将服务器上的档案删除')) return false;
			jQuery('.ui-dialog-titlebar').hide();
			jQuery('#process').dialog('open');
			jQuery('#process').html('正在处理...请稍候...<br />如果长时间无相应，可能因为服务器无法连接');
            jQuery.ajax({
                'type':'post',
                'url':'". $this->createUrl('remove') ."',
                'data':{
                    'uploadIds':upload_ids.join(','),
                    'projectId':". $currentProject->projectId ."
                },
                'dataType':'json',
                'success':function(data, status) {
                    if (data.error) {
                        alert(data.error);
						jQuery('.ui-dialog-titlebar').show();
                    }
                    else {
                        alert('成功');
                        location.href='". $this->createUrl('publog/index', array('id'=>$currentProject->projectId)) ."';
                    }
                    jQuery('#process').dialog('close');
                }
            });
		});

");
?>