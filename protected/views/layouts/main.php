<?php echo $this->renderPartial('//layouts/_header');?>
<body>
<div  id="HomePageView" class="PageView" style="overflow: auto;">
<div  id="YDefaultView" style="overflow: auto;">
<div  class="GridView" style="overflow: auto;">
<div>
  <div  id="HeaderView" style="overflow: auto;">
    <div id="MainLogo" class="svnpub-logo"></div>
    <div  class="GridView" style="overflow: auto;">
      <div>
        <div  id="HeaderLinksView" style="overflow: auto;">
          <div>
            <ul>
              <li>您好, <b><?php echo Yii::app()->user->name;?></b></li>
              <li><?php echo CHtml::link('个人设置', array('user/setting'));?></li>
              <li><?php echo CHtml::link('退出', array('site/logout'));?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    </div>
</div>
<div>
  <div id="MenuView" style="">
    <div class="nav-container">
      <div class="nav-bar">
	        <?php $this->widget('EMenu',array(
	        	'htmlOptions'=>array(
	        			'class'=>'nav'
	        	),
	        	'encodeLabel'=>false,
	        	'lastItemCssClass'=>'last',
	        	'itemCssClass'=>'parent',
	            'items'=>array(
	                array('label'=>'主页', 'url'=>array('/site/index')),
	                array('label'=>'上传', 'url'=>array('/upload/index','id'=>Yii::app()->request->getParam('id', '0')),
	                		'itemOptions'=>array(
	                				'itemCssClass'=>'',
	                		),
	                		'items'=>array(
	                				array('label'=>'<span>最新提交列表</span>', 'url'=>array('/upload/index','id'=>Yii::app()->request->getParam('id', '0'))),
	                		),
	                		
	                ),
	                array('label'=>'发布', 'url'=>array('/publish/index','id'=>Yii::app()->request->getParam('id', '0')),
	                		'itemOptions'=>array(
	                				'itemCssClass'=>'',
	                		),
	                		'items'=>array(
	                				array('label'=>'<span>最新等待发布列表</span>', 'url'=>array('/publish/index','id'=>Yii::app()->request->getParam('id', '0'))),
	                				array('label'=>'<span>发布日志列表</span>', 'url'=>array('/publog/index','id'=>Yii::app()->request->getParam('id', '0'))),
	                		),
	                	),
	            		/*
	                array('label'=>'标签', 'url'=>array('/tag/index'),
	                		'itemOptions'=>array(
	                				'itemCssClass'=>'',
	                		),
	                		'items'=>array(
	                				array('label'=>'<span>添加标签</span>', 'url'=>array('/tag/create')),
	                		),
	                	),*/
            		array('label'=>'项目', 'url'=>array('/project/index'),
            				'visible'=>Yii::app()->user->role=='admin',
            				'itemOptions'=>array(
            						'itemCssClass'=>'',
            				),
            				'items'=>array(
            						array('label'=>'<span>添加项目</span>', 'url'=>array('/project/create')),
            						array('label'=>'<span>项目列表</span>', 'url'=>array('/project/index')),
            				),
            		),
	                array('label'=>'主机', 
	                		'url'=>array('/host/index'),
	                		'visible'=>Yii::app()->user->role=='admin',
	                		'itemOptions'=>array(
	                				'itemCssClass'=>'',
	                		),
	                		'items'=>array(
	                					array('label'=>'<span>添加主机</span>', 'url'=>array('/host/create')),
	                					array('label'=>'<span>主机列表</span>', 'url'=>array('/host/index')),
	                				)
	                		),
            		array('label'=>'用户', 'url'=>array('/user/index'),
            				'visible'=>Yii::app()->user->role=='admin',
            				'itemOptions'=>array(
            						'itemCssClass'=>'',
            				),
            				'items'=>array(
            						array('label'=>'<span>添加用户</span>', 'url'=>array('/user/create')),
            						array('label'=>'<span>用户列表</span>', 'url'=>array('/user/index')),
            				),
            		),
	            ),  
	        )); ?>
      </div>
    </div>
  </div>
</div>

<?php echo $content;?>
<?php echo $this->renderPartial('/layouts/_footer');?>
</div></div></div>
<?php Yii::app()->getClientScript()->registerScript('nav', '
$(".nav li").hover(
	function () {
		if ($(this).hasClass("parent")) {
			$(this).addClass("over");
		}
	},
	function () {
		$(this).removeClass("over");
	}
);
');

Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/css/scrolltopcontrol.js', CClientScript::POS_END);
?>
</body></html>
