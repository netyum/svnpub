<?php
$this->pageTitle=Yii::app()->name . ' - 查看主机';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>查看主机 <?php echo $model->hostText; ?></h1>
			</div>
		</div>
		<div class="MetadataView">
      <div class="view-toolbar">
          <?php echo CHtml::link('返回', array('/host/index'));?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'hostText'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'hostText'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'hostType'); ?></th>
              <td colspan="1">
              <?php echo Host::$typeName[CHTML::value($model, 'hostType')]; ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'host'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'host'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'port'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'port'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'username'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'username'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'pubPath'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'pubPath'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'archivePath'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'archivePath'); ?>
              </td>
              <th><?php echo CHtml::activeLabel($model,'tmpPath'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'tmpPath'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo CHtml::activeLabel($model,'scriptLogPath'); ?></th>
              <td colspan="1">
              <?php echo CHTML::value($model, 'scriptLogPath'); ?>
              </td>
              <th>-</th>
              <td colspan="1">
              -
              </td>
            </tr>
          <?php if (!is_array($model->distributes) && count($model->distributes)>0) :?>
            <tr>
              <th><?php echo CHtml::label('分发服务器', 'di'); ?></th>
              <td colspan="3">
		        <table>
		          <colgroup>
		          <col style="width:10%">
		          <col style="width:10%">
		          <col style="width:20%">
		          <col style="width:20%">
		          <col style="width:20%">
		          <col style="width:20%">
		          </colgroup>
		          <tbody>
		            <tr>
		              <th><?php echo $model->getAttributeLabel('host'); ?></th>
		              <th><?php echo $model->getAttributeLabel('port'); ?></th>
		              <th><?php echo $model->getAttributeLabel('pubPath'); ?></th>
		              <th><?php echo $model->getAttributeLabel('archivePath'); ?></th>
		              <th><?php echo $model->getAttributeLabel('tmpPath'); ?></th>
		              <th><?php echo $model->getAttributeLabel('scriptLogPath'); ?></th>
		            </tr>
		          <?php foreach($model->distributes as $di) :?>
		            <tr>
		              <th><?php echo $di->host; ?></th>
		              <th><?php echo $di->port; ?></th>
		              <th><?php echo $di->pubPath; ?></th>
		              <th><?php echo $di->archivePath; ?></th>
		              <th><?php echo $di->tmpPath; ?></th>
		              <th><?php echo $di->scriptLogPath; ?></th>
		            </tr>
		          <?php endforeach;?>
		          </tbody>
		        </table>
              </td>
            </tr>
		<?php endif;?>
          </tbody>
        </table>
		
		</div>
	</div>

</div>