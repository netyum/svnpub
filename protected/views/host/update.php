<?php
$this->pageTitle=Yii::app()->name . ' - 修改主机';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>修改主机 <?php echo $model->hostText; ?></h1>
			</div>
		</div>
		<div class="MetadataView">

<?php echo $this->renderPartial('_form', 
	array(
			'model'=>$model,
			'distributeHostList'=>$distributeHostList
	    )); ?>

		
		</div>
	</div>

</div>