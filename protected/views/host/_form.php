    <div class="wide form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'user-form',
                'enableAjaxValidation'=>false,
        )); ?>
      <div class="view-toolbar">
          <?php echo CHtml::link('取消', array('/host/index'));?>&nbsp;|&nbsp;
          <?php echo CHtml::submitButton($model->isNewRecord ? '添加' : '保存'); ?>
        </div>
        <table>
          <colgroup>
          <col style="width:20%">
          <col style="width:30%">
          <col style="width:20%">
          <col style="width:30%">
          </colgroup>
          <tbody>
            <tr>
              <th><?php echo $form->labelEx($model,'hostText'); ?></th>
              <td colspan="1">
              <?php echo $form->textField($model,'hostText',array('size'=>60,'maxlength'=>50)); ?>
              <br />(在发布及项目中选择主机时用来区分的标识，主机用在区分相同主机不同配置时）
              <?php echo $form->error($model,'hostText'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'hostType'); ?></th>
              <td colspan="1">
              <?php echo $form->dropDownList($model,'hostType',array('1'=>'生产','0'=>'测试')); ?>
              <br />(主机类型，包括生产和测试两种)
              <?php echo $form->error($model,'$hostType'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'host'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'host',array('size'=>60,'maxlength'=>100)); ?>
				<br />(主机的域名或IP)
				<?php echo $form->error($model,'host'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'port'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'port',array('size'=>60,'maxlength'=>100)); ?>
				<br />(主机的ssh端口)
				<?php echo $form->error($model,'port'); ?>
			</td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'username'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100)); ?>
				<br />(主机的ssh帐号)
				<?php echo $form->error($model,'username'); ?>
			</td>
              <th><?php echo $form->labelEx($model,'password'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'password',array('size'=>60,'maxlength'=>100)); ?>
				<br />(主机的ssh密码)
				<?php echo $form->error($model,'password'); ?>
              </td>
            </tr>
            <tr>
              <th><?php echo $form->labelEx($model,'pubPath'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'pubPath',array('size'=>60,'maxlength'=>100)); ?>
				<br />(发布路径，SSH用户需要有对这个路径的可写权限)
				<?php echo $form->error($model,'pubPath'); ?>
			 </td>
              <th><?php echo $form->labelEx($model,'archivePath'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'archivePath',array('size'=>60,'maxlength'=>100)); ?>
				<br />(档案存放路径，用于回滚操作使用， SSH用户需要有对这个路径的可写权限)
				<?php echo $form->error($model,'archivePath'); ?>
              </td>
            </tr>
            
            <tr>
              <th><?php echo $form->labelEx($model,'scriptLogPath'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'scriptLogPath',array('size'=>60,'maxlength'=>100)); ?>
				<br />(发布脚本执行日志，存放路径)
				<?php echo $form->error($model,'scriptLogPath'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'tmpPath'); ?></th>
              <td colspan="1">
				<?php echo $form->textField($model,'tmpPath',array('size'=>60,'maxlength'=>100)); ?>
				<br />(临时目录，用于发布服务器与分发服务器数据交互，如果修改，将发布及分发服务器)
				<?php echo $form->error($model,'tmpPath'); ?>
			 </td>
            </tr>
            
            <tr>
              <th><?php echo $form->labelEx($model,'sendStatus'); ?></th>
              <td colspan="1">
              <?php echo $form->textField($model,'sendStatus',array('size'=>60,'maxlength'=>50)); ?>
              <br />(在发布及项目中选择主机时用来区分的标识，主机用在区分相同主机不同配置时）
              <?php echo $form->error($model,'sendStatus'); ?>
              </td>
              <th><?php echo $form->labelEx($model,'sendStatus'); ?></th>
              <td colspan="1">
              <?php echo $form->dropDownList($model,'sendStatus',array('1'=>'是','0'=>'否')); ?>
              <?php echo $form->error($model,'$sendStatus'); ?>
              </td>
            </tr>            
            
            <tr>
              <th><?php echo CHtml::label('分发主机列表','distributeHostList');?></th>
              <td colspan="1">
				<?php echo CHtml::textArea('distributeHostList', $distributeHostList, array('rows'=>10,'cols'=>40)); ?>
              </td>
              <td colspan="2">
				分发服务器列表列表格式<br />
				主机:端口:发布路径:档案路径:临时路径:日志路径<br />
				例如：192.168.0.200:22:/var/www/html:/var/archive:/tmp:/var/log/publog<br />
				一行一条，不设置可留空，将使用发布机配置
			 </td>
            </tr>
          </tbody>
        </table>
    </div>
    <?php $this->endWidget(); ?>
  </div>