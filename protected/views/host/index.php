<?php
$this->pageTitle=Yii::app()->name . ' - 主机列表';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>主机列表</h1>
			</div>
		</div>
		<div class="MetadataView">
		
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'host-grid',
			'htmlOptions'=>array(
					'class'=>'cgrid-view'
			),
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
				array('header'=>'主机类型', 'name'=>'hostType', 'filter'=>Host::$typeName, 'value'=>array('Host','showType')),
			    array('header'=>'主机标识', 'name'=>'hostText'),
				array('header'=>'主机地址', 'name'=>'host'),
				array('header'=>'主机端口', 'name'=>'port'),
				array('header'=>'发布路径', 'name'=>'pubPath'),
				array('header'=>'ssh帐号', 'name'=>'username'),
				array(
					'header'=>'操作',
					'class'=>'CButtonColumn',
				),
			),
		)); ?>
		
		</div>
	</div>

</div>