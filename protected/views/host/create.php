<?php
$this->pageTitle=Yii::app()->name . ' - 添加主机';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>添加主机</h1>
			</div>
		</div>
		<div class="MetadataView">

		<?php echo $this->renderPartial('_form', 
			array(
				'model'=>$model,
				'distributeHostList'=>$distributeHostList
			)); ?>
		
		</div>
	</div>

</div>