<?php
$this->pageTitle=Yii::app()->name . ' - '. $currentProject->projectName .'- 最新提交文件列表';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1><?php echo $currentProject->projectName;?> 最新提交文件列表</h1>
			</div>
		</div>
		<div class="MetadataView">
      	<div class="view-toolbar">
          <?php echo CHtml::button('选择其它项目', array('class'=>'select-project'));?>&nbsp;
          <?php echo CHtml::button('上传到服务器', array('class'=>'upload')); ?>&nbsp;
          <?php echo CHtml::button('放入发布列表', array('class'=>'addpub')); ?>
        </div>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'upload-grid',
			'selectableRows'=>2,
			'htmlOptions'=>array(
					'class'=>'cgrid-view'
			),
			'dataProvider'=>$model->search($currentProject->projectId, $currentProject->svnProjectName),
			'filter'=>$model,
			'columns'=>array(
					array(
							'class'=>'CCheckBoxColumn'
					),
					array('header'=>'文件', 'name'=>'filename', 'value'=>'Tools::checkPath($data->filename,"'. $currentProject->svnProjectName .'")'),
					array('header'=>'提交者','name'=>'author'),
					array('header'=>'提交日志','name'=>'logMessage'),
					array('header'=>'是否上传', 
							'name'=>'isUpload', 
							'filter'=>Upload::$showUpload, 'value'=>'Upload::$showUpload[$data->isUpload]'),
					array(
							'header'=>'提交时间',
							'name'=>'updateTime',
							'filter'=>'single',
							'language'=>'zh_cn',
							'class'=>'SYDateColumn'
					),
			),
		)); ?>
		
		</div>
	</div>

</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'=>'select_project',
        // additional javascript options for the dialog plugin
        'options'=>array(
                'title'=>'选择项目',
                'resizable'=>false,
                'autoOpen'=>false,
                'disabled'=>true,
                'modal'=> true,
                'closeOnEscape'=>false,
                'draggable'=>false,
                'height'=>100,
        ),
)); 

foreach($projects as $p) :
	$url = $this->createUrl('index', array('id'=>$p->projectId));
	$link = CHtml::link($p->projectName, $url);
	echo CHtml::tag('span', array(), $link);
	echo '&nbsp;';
	echo CHtml::closeTag('span');
endforeach;

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'=>'upload',
        // additional javascript options for the dialog plugin
        'options'=>array(
                'title'=>'选择服务器',
                'resizable'=>false,
                'autoOpen'=>false,
                'disabled'=>true,
                'modal'=> true,
                'closeOnEscape'=>false,
                'draggable'=>false,
                'height'=>100,
        ),
)); 
$keys = array_keys($hostListData);
echo CHtml::dropDownList('hostId', array_shift($keys), $hostListData);
echo CHtml::button('上传', array('class'=>'start-upload'));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'=>'process',
        // additional javascript options for the dialog plugin
        'options'=>array(
                'title'=>false,
                'resizable'=>false,
                'autoOpen'=>false,
                'disabled'=>true,
                'modal'=> true,
                'closeOnEscape'=>false,
                'draggable'=>false,
        		'width'=>350,
                'height'=>100,
        ),
)); 
?>
正在处理...请稍候...<br />
如果长时间无相应，可能因为服务器无法连接<br />
请检查项目配置
<?php 
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<?php 
Yii::app()->clientScript->registerScript('upload', "
		var upload_ids = new Array();
		$('.select-project').live('click',function(){
			$('#select_project').dialog('open');
		});
		
		$('.upload').live('click',function(){
			var select_checked_num = jQuery('input.select-on-check:checked').length;
	
			if (select_checked_num==0) {
				alert('请选择要上传的文件');return false;
			}

            jQuery('input.select-on-check:checked').each(function() {
                upload_ids.push(jQuery(this).val());
            });
			if (!confirm('请确认，要将选中文件上传到服务器')) return false;
			$('#upload').dialog('open');
		});
		
		$('.addpub').live('click', function() {
			var select_checked_num = jQuery('input.select-on-check:checked').length;
	
			if (select_checked_num==0) {
				alert('请选择要加入的文件');return false;
			}

            jQuery('input.select-on-check:checked').each(function() {
                upload_ids.push(jQuery(this).val());
            });
			if (!confirm('请确认，要将选中文件放入发布列表中, 如果选中文件还没有上传，将不能放入')) return false;
			//$('#upload').dialog('open');
			jQuery('.ui-dialog-titlebar').hide();
			jQuery('#upload').dialog('close');
			jQuery('#process').dialog('open');
			jQuery('#process').html('正在处理...请稍候...');
			jQuery.ajax({
				'type':'post',
				'url':'". $this->createUrl('addpub') ."',
				'data':{
                    'uploadIds':upload_ids.join(','),
                    'projectId':". $currentProject->projectId ."
				},
                'dataType':'json',
                'success':function(data, status) {
                    if (data.error) {
                        alert(data.error);
						jQuery('.ui-dialog-titlebar').show();
                    }
                    else {
                        alert('成功');
                        location.href='". $this->createUrl('upload/index', array('id'=>$currentProject->projectId)) ."';
                    }
                    jQuery('#process').dialog('close');
                }
			});
		});
		
		$('.start-upload').live('click', function(){
			var host_id = jQuery('#hostId').val();
			jQuery('.ui-dialog-titlebar').hide();
			jQuery('#upload').dialog('close');
			jQuery('#process').dialog('open');
			jQuery('#process').html('正在处理...请稍候...<br />如果长时间无相应，可能因为服务器无法连接<br />请检查项目配置');
            jQuery.ajax({
                'type':'post',
                'url':'". $this->createUrl('upload') ."',
                'data':{
                    'uploadIds':upload_ids.join(','),
                    'projectId':". $currentProject->projectId .",
					'hostId':host_id
                },
                'dataType':'json',
                'success':function(data, status) {
                    if (data.error) {
                        alert(data.error);
						jQuery('.ui-dialog-titlebar').show();
                    }
                    else {
                        alert('成功');
                        location.href='". $this->createUrl('upload/index', array('id'=>$currentProject->projectId)) ."';
                    }
                    jQuery('#process').dialog('close');
                }
            });
		});
");
?>