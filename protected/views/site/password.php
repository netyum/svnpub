<?php
$this->breadcrumbs=array(
	'密码'=>array('password'),
	'修改',
);

?>

<h1>修改密码 <?php echo Yii::app()->user->name; ?></h1>

<?php if(Yii::app()->user->hasFlash('password')): ?>

<div class="flash-success" id="flash-success">
	<?php echo Yii::app()->user->getFlash('password'); ?>
</div>
<script type="text/javascript">
jQuery('#flash-success').fadeOut(2000);
</script>
	
<?php endif;?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'password-form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
)); ?>
	<p class="note">带 <span class="required">*</span> 必填。</p>
	
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'olduserPassword'); ?>
		<?php echo $form->passwordField($model,'olduserPassword',array('size'=>50,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'olduserPassword'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'userPassword'); ?>
		<?php echo $form->passwordField($model,'userPassword',array('size'=>50,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'userPassword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reuserPassword'); ?>
		<?php echo $form->passwordField($model,'reuserPassword',array('size'=>50,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'reuserPassword'); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton('修改'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
