<?php $this->pageTitle=Yii::app()->name; ?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>欢迎使用<?php echo CHtml::encode(Yii::app()->name); ?></h1>
			</div>
		</div>
		<div class="MetadataView">
			<p>上传流程如下：</p>
			
			<ol>
				<li>点击上传菜单进入最新提交文件列表</li>
				<li>选择需要测试的项目</li>
				<li>在列出最近提交文件列表中选定要上传的文件</li>
				<li>选择上传测试服务器</li>
				<li>提交上传（提交后，文件列表中的文件不会消失）</li>
				<li>测试没有问题可放入发布列表</li>
			</ol>
			
			<p>发布流程如下：</p>
			
			<ol>
				<li>点击发布菜单进入发布列表</li>
				<li>选择需要发布的项目</li>
				<li>在列出最近提交未上传的文件列表中选定要发布的文件</li>
				<li>填写发布备注信息</li>
				<li>选择发布上线服务器</li>
				<li>提交发布（提交后，发布文件列表文件会消失，</li>
			</ol>
			
			<h2>更新历史</h2>
			
			<p>2012-2-26：</p>
			<ol>
				<li>更新界面，重构本系统</li>
			</ol>
		</div>
	</div>

</div>