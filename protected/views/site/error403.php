<?php
$this->pageTitle=Yii::app()->name . ' - 你没有访问此页面的授权.';
?>
<div>
	<div class="GridView" style="overflow: auto;">
		<div>
			<div style="" class="TitleBarView">
				<h1>你没有访问此页面的授权.</h1>
			</div>
		</div>
		<div class="MetadataView">
		<?php echo CHtml::encode($message); ?>
		
		</div>
	</div>

</div>