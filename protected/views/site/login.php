<?php echo $this->renderPartial('//layouts/_header');?>
<body>
<div  id="LoginPageView" class="PageView" style="overflow: auto;">
  <div  class="GridView" style="overflow: auto;">
    <div>
      <div  id="LoginView" style="overflow: auto;">
        <div id="LoginLogo" class="svnpub-logo"></div>
        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableAjaxValidation'=>false,
        )); ?>
            <div>
              <?php echo $form->labelEx($model,'username'); ?>
              <?php echo $form->textField($model,'username'); ?>
              <?php echo $form->error($model,'username'); ?>
            </div>
            <div>
              <?php echo $form->labelEx($model,'password'); ?>
              <?php echo $form->passwordField($model,'password'); ?>
              <?php echo $form->error($model,'password'); ?>
            </div>
            <div style = 'clear:both;'>
              <?php echo CHtml::submitButton('登录'); ?>
            </div>
    	<?php $this->endWidget(); ?>
        </div>
    </div>
	<?php echo $this->renderPartial('//layouts/_footer');?>
  </div>
</div>

</body>
</html>
