<?php

class PubForm extends CFormModel
{
	public $reason; //发布原因
	public $project_id; //项目ID
	
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			array('reason', 'required'),

		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'reason'=>'发布原因',
		);
	}
}