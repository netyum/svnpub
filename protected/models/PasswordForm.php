<?php

/**
 * PasswordForm class.
 * 修改密码表单
 * 
 */
class PasswordForm extends CFormModel
{
	
	public $olduserPassword;
	public $userPassword;
	public $reuserPassword;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('olduserPassword, userPassword, reuserPassword', 'required'),
			array('olduserPassword', 'checkOldPassword'),
			array('reuserPassword', 'compare', 'compareAttribute'=>'userPassword', 'message'=>'两次输入的密码不一致'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'olduserPassword'=>'旧密码',
			'userPassword'=>'新密码',
			'reuserPassword'=>'确认密码',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function checkOldPassword($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$model = User::model()->find('userName=? and userPassword=?', array(Yii::app()->user->name,md5($this->olduserPassword)));
			if (is_null($model)) 
				$this->addError('olduserPassword','旧密码不正确。');
		}
	}

}
