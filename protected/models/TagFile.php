<?php

/**
 * This is the model class for table "tag_file".
 *
 * The followings are the available columns in table 'tag_file':
 * @property integer $file_id
 * @property integer $tag_id
 * @property integer $project_id
 * @property string $filename
 */
class TagFile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TagFile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tag_file';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tag_id, project_id', 'numerical', 'integerOnly'=>true),
			array('filename', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('file_id, tag_id, project_id, filename', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'file_id' => 'File',
			'tag_id' => 'Tag',
			'project_id' => 'Project',
			'filename' => 'File Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('file_id',$this->file_id);
		$criteria->compare('tag_id',$this->tag_id);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('filename',$this->filename,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}