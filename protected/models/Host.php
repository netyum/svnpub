<?php

/**
 * This is the model class for table "host".
 *
 * The followings are the available columns in table 'host':
 * @property integer $hostId
 * @property string $host
 * @property string $username
 * @property string $password
 * @property string $pubPath
 */
class Host extends CActiveRecord {

    // public $is_jump;

    /**
     * Returns the static model of the specified AR class.
     * @return Host the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{host}}';
    }
    
    public static $typeName = array('测试', '生产');

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('host, port, username, password, pubPath, hostText, archivePath, tmpPath, scriptLogPath, sendStatus', 'required'),
                array('host, username, password, pubPath, hostText, archivePath, tmpPath', 'length', 'max'=>255),
                array('hostType', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('hostId, host, username, password, pubPath', 'safe', 'on'=>'search'),
        );
    }

    public function scopes() {
        return array(
                'dropDownDataByProject'=>array(
                        'with'=>array('projects'),
                        'condition'=>'projects.projectId=:projectId'

                ),

        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'projects'=>array(self::MANY_MANY, 'Project', '{{project_host}}(hostId, projectId)'),
                'users'=>array(self::MANY_MANY, 'User', '{{user_host}}(hostId, userId)'),
        		'distributes'=>array(self::HAS_MANY, 'DistributeHost', 'hostId')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'hostId' => 'Host',
                'hostType' => '主机类型',
        		'port' => '端口',
                'host' => '主机地址',
                'username' => '帐号',
                'password' => '密码',
                'pubPath' => '发布路径',
                'hostText'=>'主机标识',
                'archivePath'=>'档案存放目录',
                'tmpPath'=>'临时文件存放目录',
                'scriptLogPath'=>'脚本执行日志存放路径',
        		'sendStatus'=>'代码是否在当前主机发布',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('hostId',$this->hostId);
        $criteria->compare('host',$this->host,true);
        $criteria->compare('port',$this->port,true);
        $criteria->compare('hostType',$this->hostType);
        $criteria->compare('hostText',$this->hostText,true);
        $criteria->compare('pubPath',$this->pubPath,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);

        return new CActiveDataProvider(get_class($this), array(
                        'criteria'=>$criteria,
        ));
    }

    public static function showType($data, $row, $c) {
    
    	return self::$typeName[$data->hostType];

    }
    
    public function insertDistributeHost($distributeHostList='')
    {
    	if ($distributeHostList!='') {
    		$lists = explode("\n", $distributeHostList);
    		if (is_array($lists) && count($lists)>0) {
    			foreach($lists as $list) {
    				$hostInfo = explode(':', trim($list));
    				if (count($hostInfo)!=6) continue;
    				$data = array(
    						'host'=>$hostInfo[0],
    						'port'=>trim($hostInfo[1])=='' ? $model->port : $hostInfo[1],
    						'pubPath'=>trim($hostInfo[2])=='' ? $model->pubPath : $hostInfo[2],
    						'archivePath'=>trim($hostInfo[3])=='' ? $model->archivePath : $hostInfo[3],
    						'tmpPath'=>trim($hostInfo[4])=='' ? $model->tmpPath : $hostInfo[4],
    						'scriptLogPath'=>trim($hostInfo[5])=='' ? $model->scriptLogPath : $hostInfo[5],
    						'hostId' => $this->hostId
    				);
    				Yii::app()->db->createCommand()->insert('{{distribute_host}}', $data);
    			}
    		}
    	}
    }
    
    public function deleteDistributeHost() {
    	Yii::app()->db->createCommand()->delete('{{distribute_host}}', 'hostId=?', array($this->hostId));
    }

}
