<?php

/**
 * This is the model class for table "tag".
 *
 * The followings are the available columns in table 'tag':
 * @property integer $tag_id
 * @property string $name
 * @property integer $creator
 * @property integer $developer
 * @property string $createdate
 * @property string $remark
 */
class Tag extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tag';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('creator, developer', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('remark', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tag_id, name, creator, developer, createdate, remark', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'tagfiles'=>array(self::HAS_MANY, 'TagFile', 'tag_id'),
		//    'hosts'=>array(self::MANY_MANY, 'Host', 'user_host(userId, host_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tag_id' => 'Tag',
			'name' => '标签名称',
			'creator' => '创建者',
			'developer' => '开发人员',
			'createdate' => '创建时间',
			'remark' => '备注',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tag_id',$this->tag_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('creator',$this->creator);
		$criteria->compare('developer',$this->developer);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('remark',$this->remark,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
    
    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
    public function uploadSearch($tag_id=0, $project_id=0, $svn_project_name='')
	{
		$criteria=new CDbCriteria;
		$criteria->order='update_time DESC';
        $uploadModel = new Upload();

		if ($project_id!=0) {
			$criteria->addCondition('t.project_id=:project_id');
			$criteria->addCondition('is_pub=0');
			$criteria->addCondition('status<>"D"');
            $criteria->addCondition('tag_file.tag_id=:tag_id');
			$criteria->params=array('project_id'=>$project_id, 'tag_id'=>$tag_id);
            $criteria->with = 'tag_file';

			if ($svn_project_name!='') {
				$criteria->addCondition('tag_file.filename LIKE "'. $svn_project_name .'/%"');
			}
		}

		$criteria->compare('filename',$this->unfilterlog($uploadModel->filename), true);
		$criteria->compare('logmessage', $this->unfilterlog($uploadModel->logmessage), true);
//		$criteria->compare('logmessage',$this->logmessage,true);
		$criteria->compare('author',$uploadModel->author,true);

	//	$criteria->limit = 100;
        
		return new CActiveDataProvider(get_class($uploadModel), array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50
			)
		));
	}

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
    public function pubSearch($tag_id=0, $project_id=0, $svn_project_name='')
	{
		$criteria=new CDbCriteria;
		$criteria->order='update_time DESC';
        $pubModel = new Pub();
        
		if ($project_id!=0 && $tag_id != 0) {
			$criteria->addCondition('t.project_id=:project_id');
			$criteria->addCondition('is_pub=0');
			$criteria->addCondition('status<>"D"');
            $criteria->addCondition('tag_file.tag_id=:tag_id');
			$criteria->params=array('project_id'=>$project_id, 'tag_id'=>$tag_id);
            $criteria->with = 'tag_file';

			if ($svn_project_name!='') {
				$criteria->addCondition('tag_file.filename LIKE "'. $svn_project_name .'/%"');
			}
		}

		$criteria->compare('filename',$this->unfilterlog($pubModel->filename), true);
		$criteria->compare('logmessage', $this->unfilterlog($pubModel->logmessage), true);
//		$criteria->compare('logmessage',$this->logmessage,true);
		$criteria->compare('author',$pubModel->author,true);

	//	$criteria->limit = 100;

		return new CActiveDataProvider(get_class($pubModel), array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50
			)
		));
	}

    public function unfilterlog($name)
	{
		$len = strlen($name);
		$s = "";
		for($i=0; $i<$len; $i++)
		{
			if (ord($name{$i})>127) $s .= "?".ord($name{$i});
			else $s.=$name{$i};
		}
		return $s;
	}
}