<?php

/**
 * This is the model class for table "project".
 *
 * The followings are the available columns in table 'project':
 */
class Project extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{project}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('projectName, svnProjectName, svnProjectPath', 'required'),
			array('projectName, svnProjectName, svnProjectPath', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
		    array('svnUser, svnPass, noPubList, isRemove', 'safe'),
			array('projectId, projectName, svnProjectName, svnProjectPath, isRemove', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'users'=>array(self::MANY_MANY, 'User', '{{project_user}}(projectId, userId)'),
		    'hosts'=>array(self::MANY_MANY, 'Host', '{{project_host}}(projectId, hostId)'),
		);
	}

	public function scopes() {
		return array(
		    'dropDownDataByUser'=>array(
				'select'=>'projectId,projectName',
				'with'=>'users',
		    )
		);
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'projectId' => 'Project',
			'projectName' => '项目名称',
			'svnProjectName' => 'Svn项目名',
			'svnProjectPath' => 'Svn项目地址',
			'svnUser'=>'Svn帐号',
			'svnPass'=>'Svn密码',
			'isRemove'=>'删除Svn文件',
			'noPubList'=>'不发布列表'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('projectId',$this->projectId);
		$criteria->compare('projectName',$this->projectName,true);
		$criteria->compare('svnProjectName',$this->svnProjectName,true);
		$criteria->compare('svnProjectPath',$this->svnProjectPath,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}