<?php
class Upload extends CActiveRecord
{

	public $updateTime_range = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @return Upload the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static $showUpload = array('否', '是');
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{upload}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filename, author, updateTime, status', 'required'),
			array('projectId, isPub', 'numerical', 'integerOnly'=>true),
			array('filename, author, updateTime, status', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('update_id, filename, author, update_time, project_id, is_pub, status', 'safe', 'on'=>'search'),
			array('updateTime_range, isUpload, updateId, filename, author, updateTime, projectId, isPub, status, logMessage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'tag_file'=>array(self::BELONGS_TO, 'TagFile', '', 'on'=>'t.filename=tag_file.filename and t.project_id=tag_file.project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'updateId' => '上传',
			'filename' => '文件名',
			'author' => '提交者',
			'updateTime' => '提交时间',
			'projectId' => '项目ID',
			'isPub' => 'IsPub',
			'status' => '提交状态',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($projectId=0, $svnProjectName='')
	{

		$criteria=new CDbCriteria;
		if ($projectId!=0) {
			$criteria->addCondition('projectId=:projectId');
			$criteria->addCondition('status<>"D"');
			$criteria->params=array(':projectId'=>$projectId);
			
			if ($svnProjectName!='') {
				$criteria->addCondition('filename LIKE "'. $svnProjectName .'/%"');
			}
			
		}
		
		$criteria->compare('filename', $this->filename, true);
		$criteria->compare('logMessage',$this->logMessage, true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('isUpload',$this->isUpload);
		//updateTime
		$to = '';
		if (isset($this->updateTime_range['to'])) {
			$to= $this->updateTime_range['to'];
		}
		if ($to!='') {
			$updateTime = date("Y-m-d", strtotime($to));
			$criteria->addCondition('t.updateTime LIKE "'. $updateTime .'%"');
		}

		if (!isset($_GET[get_class($this).'_sort']))
			$criteria->order='t.updateTime DESC';

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>50
			)
		));
	}

}
