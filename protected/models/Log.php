<?php

/**
 * This is the model class for table "log".
 *
 * The followings are the available columns in table 'log':
 * @property integer $log_id
 * @property string $author
 * @property integer $project_id
 * @property string $archive
 * @property integer $addtime
 * @property integer $host_id
 */
class Log extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Log the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('log_id, author, project_id, archive, addtime, host, reason', 'required'),
			array('log_id, project_id, addtime, host_id', 'numerical', 'integerOnly'=>true),
			array('author', 'length', 'max'=>255),
			array('archive', 'length', 'max'=>10),
		        array('filelist, host_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('log_id, author, project_id, archive, addtime, host_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'project'=>array(self::BELONGS_TO, 'Project', 'project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'log_id' => 'Log',
			'author' => '发布者',
			'project_id' => 'Project',
			'archive' => '档案名',
			'addtime' => '发布时间',
			'host' => '发布主机',
		        'filelist'=>'文件列表',
		        'reason'=>'发布原因',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($project_id=0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if ($project_id!=0) {
			$criteria->addCondition('project_id=:project_id');
			$criteria->params=array('project_id'=>$project_id);
			$criteria->order='addtime DESC';
		}

		$criteria->compare('author',$this->author,true);
		$criteria->compare('archive',$this->archive,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}