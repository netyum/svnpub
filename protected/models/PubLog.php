<?php

class PubLog extends CActiveRecord
{

	public $createTime_range = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @return Log the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pub_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('logId, pubUser, projectId, archive, hostId, fileList, host, reason', 'required'),
			array('logId, projectId, hostId', 'numerical', 'integerOnly'=>true),
			array('archive', 'length', 'max'=>255),
		    array('fileList, hostId', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('logId, pubUser, projectId, archive, createTime, createTime_range, hostId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'project'=>array(self::BELONGS_TO, 'Project', 'projectId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'logId' => 'Log',
			'pubUser' => '发布者',
			'projectId' => '项目Id',
			'archive' => '档案名',
			'createTime' => '发布时间',
			'host' => '发布主机',
		    'fileList'=>'文件列表',
		    'reason'=>'发布原因',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($projectId=0)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;
		if ($projectId!=0) {
			$criteria->addCondition('projectId=:projectId');
			$criteria->params=array('projectId'=>$projectId);
		}
		
		$criteria->compare('pubUser',$this->pubUser,true);
		$criteria->compare('archive',$this->archive,true);

		//createTime
		$to = '';
		if (isset($this->createTime_range['to'])) {
			$to= $this->createTime_range['to'];
		}

		if ($to!='') {
			$createTime = date("Y-m-d", strtotime($to));
			$criteria->compare('createTime',$createTime,true);
		}

		if (!isset($_GET[get_class($this).'_sort']))
			$criteria->order='t.createTime DESC';
		
		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}