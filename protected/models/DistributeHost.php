<?php

/**
 * This is the model class for table "{{distribute_host}}".
 *
 * The followings are the available columns in table '{{distribute_host}}':
 * @property integer $distId
 * @property string $host
 * @property integer $port
 * @property string $pubPath
 * @property string $archivePath
 * @property string $tmpPath
 * @property string $scriptLogPath
 * @property integer $hostId
 */
class DistributeHost extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DistributeHost the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{distribute_host}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('host, pubPath, archivePath, scriptLogPath, hostId', 'required'),
			array('port, hostId', 'numerical', 'integerOnly'=>true),
			array('host, pubPath, archivePath, tmpPath, scriptLogPath', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('distId, host, port, pubPath, archivePath, tmpPath, scriptLogPath, hostId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'distId' => 'Dist',
			'host' => 'Host',
			'port' => 'Port',
			'pubPath' => 'Pub Path',
			'archivePath' => 'Archive Path',
			'tmpPath' => 'Tmp Path',
			'scriptLogPath' => 'Script Log Path',
			'hostId' => 'Host',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('distId',$this->distId);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('port',$this->port);
		$criteria->compare('pubPath',$this->pubPath,true);
		$criteria->compare('archivePath',$this->archivePath,true);
		$criteria->compare('tmpPath',$this->tmpPath,true);
		$criteria->compare('scriptLogPath',$this->scriptLogPath,true);
		$criteria->compare('hostId',$this->hostId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}