<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ProjectForm extends CFormModel
{
	public $project_id;
	public $project_name;
	public $svn_project_name;
	public $svn_project_path;
	public $svn_user;
	public $svn_pass;
	public $nopub_list;
	public $is_remove;
	public $test_host;
	public $prod_host;
	 
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
		    array('project_name, svn_project_name, svn_project_path', 'required'),
		    array('test_host', 'required', 'message'=>'{attribute} 至少选择一个'),
		    array('project_name, svn_project_name, svn_project_path', 'length', 'max'=>255),
		    array('project_id, project_name, svn_project_name, svn_project_path, svn_user, svn_pass, nopub_list, is_remove, test_host, prod_host', 'safe'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'project_id' => 'Project',
			'project_name' => '项目名称',
			'test_host' => '测试主机',
			'prod_host' => '生产主机',
			'distribute_host_list' => '分发地址及路径列表',
			'svn_project_name' => 'Svn项目名',
			'svn_project_path' => 'Svn项目地址',
		        'nopub_list'=>'不发文件布列表',
		        'is_remove'=>'删除svn中已删除的文件'
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','用户名或密码不正确。');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
