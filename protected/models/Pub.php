<?php

/**
 * This is the model class for table "pub".
 *
 * The followings are the available columns in table 'pub':
 * @property integer $pub_id
 * @property string $filename
 * @property string $author
 * @property string $update_time
 * @property integer $project_id
 * @property integer $is_pub
 * @property string $status
 */
class Pub extends CActiveRecord
{

        public $update_time_range = array();

	/**
	 * Returns the static model of the specified AR class.
	 * @return Pub the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pub';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filename, author, update_time, status', 'required'),
			array('project_id, is_pub', 'numerical', 'integerOnly'=>true),
			array('filename, author, update_time, status', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('update_time_range, pub_id, filename, logmessage, author, update_time, project_id, is_pub, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'tag_file'=>array(self::BELONGS_TO, 'TagFile', '', 'on'=>'t.filename=tag_file.filename and t.project_id=tag_file.project_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pub_id' => 'Pub',
			'filename' => 'Filename',
			'author' => 'Author',
			'update_time' => 'Update Time',
			'project_id' => 'Project',
			'is_pub' => 'Is Pub',
			'status' => 'Status',
		);
	}

	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($project_id=0, $svn_project_name='')
	{

		$criteria=new CDbCriteria;
		
		if ($project_id!=0) {
			$criteria->addCondition('project_id=:project_id');
			$criteria->addCondition('is_pub=0');
			$criteria->addCondition('status<>"D"');
			$criteria->params=array('project_id'=>$project_id);
			
			if ($svn_project_name!='') {
				$criteria->addCondition('filename LIKE "'. $svn_project_name .'/%"');
			}
			
		}
		
		$criteria->order = 'update_time DESC';

                $criteria->compare('filename',$this->unfilterlog($this->filename), true);
                $criteria->compare('logmessage', $this->unfilterlog($this->logmessage), true);

		$criteria->compare('author',$this->author,true);
                

		//update_time
                $to = '';
                if (isset($this->update_time_range['to'])) {
                        $to= $this->update_time_range['to'];
                }
                if ($to!='') {
                        $update_time = date("Y-m-d", strtotime($to));
                        $criteria->compare('update_time',$update_time,true);
                }

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                                'pageSize'=>50
                        )
		));
	}


    public function unfilterlog($name)
        {
                $len = strlen($name);
                $s = "";
                for($i=0; $i<$len; $i++)
                {
                        if (ord($name{$i})>127) $s .= "?".ord($name{$i});
                        else $s.=$name{$i};
                }
                return $s;
        }
}
