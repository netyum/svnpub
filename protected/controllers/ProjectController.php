<?php

class ProjectController extends Controller
{

	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}
	
	
	public function accessRules()
	{
		return array(
				array('allow',  // allow all users to perform 'index' and 'view' actions
						'roles'=>array('admin'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*')
				)
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$hostListData = CHtml::listData(Host::model()->findAll(),'hostId','hostText', 'hostType');
		$prodHostListData = isset($hostListData[1]) ? $hostListData[1] : array(); //生产主机
		$testHostListData = isset($hostListData[0]) ? $hostListData[0] : array(); //测试主机
		
		$testHostIds = array();
		$prodHostIds = array();
		
		$model=new Project;

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if ($model->save()) {
				//主机列表
				$prodHostIds = isset($_POST['prodHostIds']) ? $_POST['prodHostIds'] : array();
				$testHostIds = isset($_POST['testHostIds']) ? $_POST['testHostIds'] : array();
				$hostIds = array_merge($prodHostIds, $testHostIds);
				if (is_array($hostIds) && count($hostIds)>0) {
					$sql = "INSERT INTO {{project_host}} (hostId, projectId) VALUES ";
					foreach($hostIds as $hostId) {
						$sql .= " ($hostId, $model->projectId),";
					}
					$sql = trim($sql, ",");
					Yii::app()->db->createCommand($sql)->execute();
				}
				
				$this->redirect(array('view','id'=>$model->projectId));
			}
		}


		$this->render('create',array(
			'model'=>$model,
			'testHostIds'=>$testHostIds,
			'prodHostIds'=>$prodHostIds,
		    'prodHostListData'=>$prodHostListData,
			'testHostListData'=>$testHostListData
		    
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$hostListData = CHtml::listData(Host::model()->findAll(),'hostId','hostText', 'hostType');
		$prodHostListData = isset($hostListData[1]) ? $hostListData[1] : array(); //生产主机
		$testHostListData = isset($hostListData[0]) ? $hostListData[0] : array(); //测试主机
		
		$testHostIds = array();
		$prodHostIds = array();
		
		//测试主机ID列表
		$testHostIds = Yii::app()->db->createCommand()
							->from('{{project_host}} t')
							->select('t.hostId')
							->join('{{host}} h', 't.hostId=h.hostId AND hostType=0')
							->where('projectId=?')
							->queryColumn(array($id));
		if (is_null($testHostIds)) $testHostIds = array();
		
		//产生主机ID列表
		$prodHostIds = Yii::app()->db->createCommand()
							->from('{{project_host}} t')
							->select('t.hostId')
							->join('{{host}} h', 't.hostId=h.hostId AND hostType=1')
							->where('projectId=?')
							->queryColumn(array($id));
		if (is_null($prodHostIds)) $prodHostIds = array();

		
		if(isset($_POST['Project']))
		{
			$model->attributes = $_POST['Project'];
			if ($model->save(false)) {
				$sql = "DELETE FROM {{project_host}} WHERE projectId=?";
				Yii::app()->db->createCommand($sql)->execute(array($model->projectId));
				
				//主机列表
				$prodHostIds = isset($_POST['prodHostIds']) ? $_POST['prodHostIds'] : array();
				$testHostIds = isset($_POST['testHostIds']) ? $_POST['testHostIds'] : array();
				$hostIds = array_merge($prodHostIds, $testHostIds);
				if (is_array($hostIds) && count($hostIds)>0) {
					$sql = "INSERT INTO {{project_host}} (hostId, projectId) VALUES ";
					foreach($hostIds as $hostId) {
						$sql .= " ($hostId, $model->projectId),";
					}
					$sql = trim($sql, ",");
					Yii::app()->db->createCommand($sql)->execute();
				}
				
				$this->redirect(array('view','id'=>$model->projectId));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'testHostIds'=>$testHostIds,
			'prodHostIds'=>$prodHostIds,
		    'prodHostListData'=>$prodHostListData,
			'testHostListData'=>$testHostListData
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			$sql = "DELETE FROM {{project_host}} WHERE projectId=?";
			Yii::app()->db->createCommand($sql)->execute(array($id));

			$sql = "DELETE FROM {{project_user}} WHERE userId=?";
			Yii::app()->db->createCommand($sql)->execute(array($id));
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('index',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Project::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
