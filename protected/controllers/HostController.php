<?php

class HostController extends Controller
{

	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);
	}
	
	
	public function accessRules()
	{
		return array(
				array('allow',  // allow all users to perform 'index' and 'view' actions
						'roles'=>array('admin'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*')
				)
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Host;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$distributeHostList = Yii::app()->request->getPost('distributeHostList', '');
		if(isset($_POST['Host']))
		{
			$model->attributes=$_POST['Host'];
			if($model->save()) {
				$model->insertDistributeHost($distributeHostList);
				$this->redirect(array('view','id'=>$model->hostId));
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
			'distributeHostList'=>$distributeHostList
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$distributeHostList = '';
		$lists = Yii::app()->db->createCommand()
						->from('{{distribute_host}}')
						->select('host, port, pubPath, archivePath, tmpPath, scriptLogPath')
						->where('hostId=?')
						->queryAll(false, array($model->hostId));
		if (count($lists)>0) {
			foreach($lists as $key => $value) {
				$lists[$key] = join(':', $value);
			}
			$distributeHostList = join("\n", $lists);
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Host']))
		{
			$model->attributes=$_POST['Host'];
			if($model->save())
				$distributeHostList = Yii::app()->request->getPost('distributeHostList', '');
				$model->deleteDistributeHost();
				$model->insertDistributeHost($distributeHostList);
				$this->redirect(array('view','id'=>$model->hostId));
		}

		$this->render('update',array(
			'model'=>$model,
			'distributeHostList'=>$distributeHostList
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			$sql = "DELETE FROM {{project_host}} WHERE hostId=?";
			Yii::app()->db->createCommand($sql)->execute(array($id));

			$sql = "DELETE FROM {{user_host}} WHERE hostId=?";
			Yii::app()->db->createCommand($sql)->execute(array($id));
			
			$sql = "DELETE FROM {{distribute_host}} WHERE hostId=?";
			Yii::app()->db->createCommand($sql)->execute(array($id));
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Host('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Host']))
			$model->attributes=$_GET['Host'];

		$this->render('index',array(
			'model'=>$model,
		));
	}



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Host::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='host-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	

}
