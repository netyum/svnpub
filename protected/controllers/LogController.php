<?php

class LogController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('admin', 'view'),
				'users'=>array('@'),
			),
		    
			
		        array('allow',
			   
				'actions'=>array('admin', 'create','update', 'delete', ),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Log;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Log']))
		{
			$model->attributes=$_POST['Log'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->log_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Log']))
		{
			$model->attributes=$_POST['Log'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->log_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		
		$dataProvider=new CActiveDataProvider('Log');
		/*, array(
		    'condition'=>'project_id=:project_id',
		    'params'=>array('project_id'=>$defaultProjectId)
		));*/
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
		//用户可用项目列表
		$modelProjects= Project::model()->findAll(array(
		    'with'=>array('users'),
		    'condition'=>'users.userId = ?',
		    'params'=>array(Yii::app()->user->id)
		));	//用户可用项目

		if (count($modelProjects)<=0) {
			echo "<script>alert('你暂时没有被分配项目');location.href='". $this->createUrl('site/index'). "';</script>";
			exit;
		}
		
		$projectListData = array();
		$projectsSvnName = array();
		$defaultProjectId = '';
		

		foreach($modelProjects as $project) {
			if ($defaultProjectId=='') $defaultProjectId = $project->project_id;
			$projectListData[$project->project_id]=$project->project_name;
			$projectsSvnName[$project->project_id]=$project->svn_project_name;
		}

		
		if (isset($_GET['project_id'])) {
			$project_id = (int) $_GET['project_id'];
			foreach($projectListData as $id=>$value) {
				if ($id==$project_id) {
					$defaultProjectId = $project_id;
					break;
				}
			}
		}
		

		//获得项目可用测试上传主机
		$modelHosts = Host::model()->findAll(array(
			'condition'=>'host_type=1',
		    	'with'=>array(
					'projects'=>array(    
						'condition'=>'projects.project_id='. $defaultProjectId,
					),	
					'users'=>array(
					    'condition'=>'users.userId='. Yii::app()->user->getId()
					)
				),
		));
		
		$hostListData = array();
		if (!is_null($modelHosts) && is_array($modelHosts)) {
			foreach($modelHosts as $modelHost) {
				$hostListData[$modelHost->host_id] = $modelHost->host_text." (".$modelHost->host.":".$modelHost->pub_path.")"; 
			}
		}
		
		$model=new Log('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Log']))
			$model->attributes=$_GET['Log'];

		$this->render('admin',array(
			'model'=>$model,
		        'defaultProjectId'=>$defaultProjectId,
		        'hostListData'=>$hostListData,
		        'projectListData'=>$projectListData
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Log::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='log-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
