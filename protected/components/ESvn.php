<?php

class ESvn extends CComponent
{
	/**
	 * svn path or url
	 * @var string
	 */
	private $_reposUrl= null;
	
	/**
	 * @var string username. SVN帐号
	 */
	private $_username;
	
	/**
	 * @var string password. SVN密码
	 */
	private $_password;
	
	
	/**
	 * set username and password
	 * @param string $username
	 * @param string $password
	 */
	public function __construct($username='',$password='') {
		$this->_username = $username;
		$this->_password = $password;
	
		$this->_setUserAndPass();
	}
	
	/**
	 * 
	 */
	protected function _setUserAndPass() {
		@svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_USERNAME, $this->_username);
		@svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_PASSWORD, $this->_password);
	}
	
	
	/**
	 * checkout
	 * @param string $reposUrl
	 * @param string $workPath
	 */
	public function checkout($reposUrl='', $workPath) {
		if ($reposUrl=='') $reposUrl = $this->_reposUrl;
		return @svn_checkout($reposUrl,  realpath($workPath));
		
	}
	
	/**
	 * checkout alias
	 * @param string $svnPath
	 * @param string $workPath
	 */
	public function co($reposUrl='', $workPath) {
		$this->checkout($reposUrl, $workPath);
	}
	
	/**
	 * svn update
	 * @param string $workPath
	 */
	public function update($workPath) {
		return @svn_update(realpath($workPath));
	}
	
	
	/**
	 * update alias
	 * @param string $workPath
	 */
	public function up($workPath) {
		$this->update($workPath);
	}
	
	/**
	 * clearup workpath
	 * @param string $workPath
	 */
	public function clearup($workPath) {
		return svn_cleanup(realpath($workPath));
	}
	
	/**
	 * commit 
	 * @param string $log
	 * @param array() $targets
	 * @param boolean $dontrecurse
	 */
	public function commit($log, $targets=array(), $dontrecurse=false) {
		return svn_commit($log , $targets, $dontrecurse);
	}

	/**
	 * commit alias
	 * @param string $log
	 * @param array() $targets
	 * @param boolean $dontrecurse
	 */
	public function ci($log, $targets=array(), $dontrecurse=false) {
		$this->commit($log , $targets, $dontrecurse);
	}
	
	/**
	 * set svn path
	 * @param string $path
	 */
	public function setReposUrl($path) {
		$this->_reposUrl = $path;
	}
	
	/**
	 * get svn path
	 */
	public function getReposUrl() {
		return $this->_reposUrl;
	}
}
