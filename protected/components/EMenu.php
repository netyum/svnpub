<?php
Yii::import('zii.widgets.CMenu');
class EMenu extends CMenu
{

	protected function renderMenu($items)
	{
		if(count($items))
		{
			echo CHtml::openTag('ul',$this->htmlOptions)."\n";
			$this->renderMenuRecursive($items);
			echo CHtml::closeTag('ul');
		}
	}
	
    /**
     * Recursively renders the menu items.
     * @param array $items the menu items to be rendered recursively
     */
    protected function renderMenuRecursive($items, $level=0, $itemOptions=array())
    {
        $count=0;
        $n=count($items);
        foreach($items as $item)
        {
            $count++;
            $class=array();
			if ($level==0) {	//顶级

				$options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
            	if($item['active'] && $this->activeCssClass!='')
            		$class[]=$this->activeCssClass;
            	if($count===1 && $this->firstItemCssClass!==null)
            		$class[]=$this->firstItemCssClass;
            	if($count===$n && $this->lastItemCssClass!==null)
            		$class[]=$this->lastItemCssClass;
            	if($this->itemCssClass!==null)
            		$class[]=$this->itemCssClass;
			}
			else {
				$options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
				if($item['active'] && isset($itemOptions['activeCssClass'])) 
					$class[]=$itemOptions['activeCssClass'];
				else if($item['active'] && $this->activeCssClass!='') 
					$class[]=$this->activeCssClass;
				
				if($count===1 && isset($itemOptions['firstItemCssClass']))
					$class[]=$itemOptions['firstItemCssClass'];
				else if($count===1 && $this->firstItemCssClass!==null)
					$class[]=$this->firstItemCssClass;
				
				if($count===$n && isset($itemOptions['lastItemCssClass']))
					$class[]=$itemOptions['lastItemCssClass'];
				else if($count===$n && $this->lastItemCssClass!==null)
					$class[]=$this->lastItemCssClass;
				
				//var_dump($item);
				if (isset($itemOptions['itemCssClass']))
					$class[]=$itemOptions['itemCssClass'];
				else if($this->itemCssClass!==null)
					$class[]=$this->itemCssClass;
			}
			
			if($class!==array())
			{
				if(empty($options['class']))
					$options['class']=implode(' ',$class);
				else
					$options['class'].=' '.implode(' ',$class);
			}

            	
			echo CHtml::openTag('li', $options);
            	
			$menu=$this->renderMenuItem($item);
			if(isset($this->itemTemplate) || isset($item['template']))
			{
				$template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
				echo strtr($template,array('{menu}'=>$menu));
			}
			else
				echo $menu;
            	
			if(isset($item['items']) && count($item['items']))
			{
				echo "\n".CHtml::openTag('ul',isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions)."\n";
				$subItemOptions = isset($item['itemOptions']) ? $item['itemOptions'] : array();
				$this->renderMenuRecursive($item['items'], 1, $subItemOptions);
				echo CHtml::closeTag('ul')."\n";
			}
            	
			echo CHtml::closeTag('li')."\n";
		}
	}
	
}