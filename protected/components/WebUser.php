<?php
class WebUser extends CWebUser
{
	public function __get($name)
	{
		if ($this->hasState('__userInfo')) {
			$user=$this->getState('__userInfo',array());
	
			if (isset($user[$name])) {
				return $user[$name];
			}
		}
	
		return parent::__get($name);
	}

	public function checkAccess($operation,$params=array(),$allowCaching=true)
	{
		if ($this->hasState('__userInfo')) {
			$user=$this->getState('__userInfo',array());
			if (isset($user['role'])) return $user['role']==$operation;
		}
		return false;

	}
}