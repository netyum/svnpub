<?php
class ESsh extends CComponent
{
	
	private $_connection = null;
	
	public function __construct($host, $port, $username, $password) {
		$this->_connection = @ssh2_connect($host, $port);
		if ($this->_connection==null) return null;
		$ret = @ssh2_auth_password($this->_connection, $username, $password);
		if (!$ret) return null;
	}
	

	public function scp($localFilename, $remoteFilename, $mode=0775) {
		return @ssh2_scp_send($this->_connection, $localFilename, $remoteFilename, $mode);
	}

	public function exec($command) {
		return @ssh2_exec($this->_connection, $command);
	}
}