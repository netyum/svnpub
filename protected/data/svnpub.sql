/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50513
 Source Host           : 127.0.0.1
 Source Database       : svnpub

 Target Server Type    : MySQL
 Target Server Version : 50513
 File Encoding         : utf-8

 Date: 03/03/2012 13:14:50 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `p_distribute_host`
-- ----------------------------
DROP TABLE IF EXISTS `p_distribute_host`;
CREATE TABLE `p_distribute_host` (
  `distId` int(10) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL COMMENT '主机地址',
  `port` int(11) NOT NULL DEFAULT '22' COMMENT '端口',
  `pubPath` varchar(255) NOT NULL COMMENT '发布路径',
  `archivePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '档案路径，存档路径，SSH用户必须有可写权限',
  `tmpPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '/tmp' COMMENT '临时文件存放路径',
  `scriptLogPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'script执行的log存放路径',
  `hostId` int(11) NOT NULL,
  PRIMARY KEY (`distId`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `p_host`
-- ----------------------------
DROP TABLE IF EXISTS `p_host`;
CREATE TABLE `p_host` (
  `hostId` int(10) NOT NULL AUTO_INCREMENT,
  `host` varchar(255) NOT NULL COMMENT '主机地址',
  `port` int(11) NOT NULL DEFAULT '22' COMMENT '端口',
  `username` varchar(255) NOT NULL COMMENT '帐号',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `pubPath` varchar(255) NOT NULL COMMENT '发布路径',
  `hostText` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主机标识，区分同主机的情况',
  `archivePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '档案路径，存档路径，SSH用户必须有可写权限',
  `tmpPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '/tmp' COMMENT '临时文件存放路径',
  `hostType` tinyint(1) NOT NULL DEFAULT '0' COMMENT '主机类型，0为测试，1为发布',
  `scriptLogPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'script执行的log存放路径',
  PRIMARY KEY (`hostId`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 COMMENT='主机列表';

-- ----------------------------
--  Table structure for `p_project`
-- ----------------------------
DROP TABLE IF EXISTS `p_project`;
CREATE TABLE `p_project` (
  `projectId` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '项目名',
  `svnProjectName` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'svn项目名',
  `svnProjectPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'svn项目路径',
  `svnUser` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `svnPass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isRemove` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否从发布服务器中，删除svn中已删除的文件',
  `noPubList` text COLLATE utf8_unicode_ci COMMENT '不发布文件列表',
  PRIMARY KEY (`projectId`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `p_project_host`
-- ----------------------------
DROP TABLE IF EXISTS `p_project_host`;
CREATE TABLE `p_project_host` (
  `hostId` int(10) NOT NULL,
  `projectId` int(10) NOT NULL COMMENT '所属项目'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `p_project_user`
-- ----------------------------
DROP TABLE IF EXISTS `p_project_user`;
CREATE TABLE `p_project_user` (
  `projectId` int(10) NOT NULL,
  `userId` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `p_pub`
-- ----------------------------
DROP TABLE IF EXISTS `p_pub`;
CREATE TABLE `p_pub` (
  `pubId` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '发布人',
  `updateTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `isPub` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已经发布',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '文件状态',
  `logMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`pubId`),
  UNIQUE KEY `f` (`filename`,`projectId`)
) ENGINE=MyISAM AUTO_INCREMENT=14448 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='更新列表';

-- ----------------------------
--  Table structure for `p_pub_log`
-- ----------------------------
DROP TABLE IF EXISTS `p_pub_log`;
CREATE TABLE `p_pub_log` (
  `logId` int(10) NOT NULL AUTO_INCREMENT,
  `pubUser` varchar(255) NOT NULL COMMENT '发布人',
  `projectId` int(10) NOT NULL,
  `archive` varchar(255) NOT NULL,
  `script` varchar(255) NOT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  `host` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '主机id',
  `reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileList` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '包含的文件列表',
  `hostId` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`logId`)
) ENGINE=MyISAM AUTO_INCREMENT=2118 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `p_tag`
-- ----------------------------
DROP TABLE IF EXISTS `p_tag`;
CREATE TABLE `p_tag` (
  `tagId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '标签名称',
  `creator` int(11) DEFAULT NULL COMMENT '创建者',
  `developer` int(11) DEFAULT NULL COMMENT '开发人员',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`tagId`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='标签表';

-- ----------------------------
--  Table structure for `p_tag_file`
-- ----------------------------
DROP TABLE IF EXISTS `p_tag_file`;
CREATE TABLE `p_tag_file` (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `tagId` int(11) DEFAULT NULL,
  `projectId` int(11) DEFAULT NULL,
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`fileId`)
) ENGINE=MyISAM AUTO_INCREMENT=158 DEFAULT CHARSET=utf8 COMMENT='标签文件表';

-- ----------------------------
--  Table structure for `p_upload`
-- ----------------------------
DROP TABLE IF EXISTS `p_upload`;
CREATE TABLE `p_upload` (
  `uploadId` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `updateTime` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `projectId` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '文件状态',
  `logMessage` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `isUpload` tinyint(4) DEFAULT '0' COMMENT '是否上传',
  PRIMARY KEY (`uploadId`),
  UNIQUE KEY `f` (`filename`,`projectId`)
) ENGINE=MyISAM AUTO_INCREMENT=17945 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='更新列表';

-- ----------------------------
--  Table structure for `p_user`
-- ----------------------------
DROP TABLE IF EXISTS `p_user`;
CREATE TABLE `p_user` (
  `userId` int(20) NOT NULL AUTO_INCREMENT,
  `defaultProject` int(10) DEFAULT '0' COMMENT '默认项目',
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` varchar(255) DEFAULT 'user',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('49', null, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@yiibook.com.com');
COMMIT;

-- ----------------------------
--  Table structure for `p_user_host`
-- ----------------------------
DROP TABLE IF EXISTS `p_user_host`;
CREATE TABLE `p_user_host` (
  `userId` int(10) NOT NULL,
  `hostId` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `p_user_tag`
-- ----------------------------
DROP TABLE IF EXISTS `p_user_tag`;
CREATE TABLE `p_user_tag` (
  `userId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

